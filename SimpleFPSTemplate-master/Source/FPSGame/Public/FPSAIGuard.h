// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "FPSAIGuard.generated.h"

class UPawnSensingComponent;

UENUM(BlueprintType)
enum class eAIState :uint8
{
	IDLE,

	ALERTED,

	SUSPICIOUS
};

UCLASS()
class FPSGAME_API AFPSAIGuard : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AFPSAIGuard();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, Category = "Components")
		UPawnSensingComponent* PawnSensingComp;


	UPROPERTY(EditInstanceOnly, Category = "AI")
		bool bPatrol;
	UPROPERTY(EditInstanceOnly, Category = "AI",meta =(EditCondition ="bPatrol"))
	AActor* FirstPatrolPoint;
	UPROPERTY(EditInstanceOnly, Category = "AI", meta = (EditCondition = "bPatrol"))
		AActor* SecondPatrolPoint;
	
		AActor* CurrentPatrolPoint;
		UFUNCTION(BlueprintCallable, Category = "AI|Navigation")
		void MoveToNextPatrolPoint();


	UFUNCTION()
		void OnAISeePawn(APawn* a_Pawn);
	UFUNCTION()
		void OnHearNoise(APawn* a_Pawn, const FVector& Location, float Volume);

	FRotator OrginalRotation;

	FTimerHandle TimerHandleForIdle;

	UFUNCTION()
		void OnDetectTimeFinish();

	UPROPERTY(ReplicatedUsing= OnRep_GuardState)
	eAIState m_eGuardState;

	UFUNCTION()
	void OnRep_GuardState();

	void UpdateState(eAIState a_eNewState);
	UFUNCTION(BlueprintImplementableEvent, Category = "AI")
		void OnGuardStateChange(eAIState a_eNewState);

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;


};
