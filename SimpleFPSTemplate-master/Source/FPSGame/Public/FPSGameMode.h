// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "FPSGameMode.generated.h"

UCLASS()
class AFPSGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:

	AFPSGameMode();

	void CompletedMission(APawn *myPawn, bool a_bMissionSucess);

	UFUNCTION(BlueprintImplementableEvent, Category ="GameMode")
	void OnMissionComplete(APawn *myPawn, bool a_bMissionSucess);

protected:
	UPROPERTY(EditDefaultsOnly, Category = "Specatating")
	TSubclassOf<AActor> SpectatingViewPointClass;


};



