// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "FPSGameMode.h"
#include "FPSHUD.h"
#include "FPSCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Kismet/GameplayStatics.h"

#include "FPSGameStateBase.h"


AFPSGameMode::AFPSGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/Blueprints/BP_Player"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AFPSHUD::StaticClass();
	GameStateClass = AFPSGameStateBase::StaticClass();
}

void AFPSGameMode::CompletedMission(APawn *myPawn, bool a_bMissionSucess)
{
	if (myPawn)
	{
		//myPawn->DisableInput(nullptr);

		APlayerController *playerController = Cast<APlayerController>(myPawn->GetController());
	
		if (playerController != nullptr)
		{
			if (SpectatingViewPointClass != nullptr)
			{
				TArray<AActor*> arySpecationgPoints;
				UGameplayStatics::GetAllActorsOfClass(this, SpectatingViewPointClass, arySpecationgPoints);
				if (arySpecationgPoints.Num() > 0)
				{
					AActor *SpecatingPoint;
					SpecatingPoint = arySpecationgPoints[0];

					//Do for all players..
					for (FConstPlayerControllerIterator it = GetWorld()->GetPlayerControllerIterator(); it; it++)
					{
						playerController = it->Get();
						if (playerController)
						{
							playerController->SetViewTargetWithBlend(SpecatingPoint, 1.5f, EViewTargetBlendFunction::VTBlend_Cubic);

						}
					}

				//	playerController->SetViewTargetWithBlend(SpecatingPoint, 1.5f, EViewTargetBlendFunction::VTBlend_Cubic);

				}
			}
			else
			{
				UE_LOG(LogTemp, Log, TEXT("No Specating Sub Class Setup!Please Add on in Game Mode!"));
			}
		}
		else
		{
			UE_LOG(LogTemp, Log, TEXT("The Player Controller Casting Result is NULL"));
		}
		

	}
	AFPSGameStateBase* GS = GetGameState<AFPSGameStateBase>();

	if (GS)
	{
		GS->MulticastOnCompletedMission(myPawn, a_bMissionSucess);
	}
	OnMissionComplete(myPawn,a_bMissionSucess);
	UE_LOG(LogTemp, Log, TEXT("AFPSGameMode:Mission Complete!"));
}

