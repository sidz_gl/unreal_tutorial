// Fill out your copyright notice in the Description page of Project Settings.

#include "FPSBlackHole.h"
#include "Components/StaticMeshComponent.h"
#include "Components/SphereComponent.h"

// Sets default values
AFPSBlackHole::AFPSBlackHole()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	OuterSphereComp = CreateDefaultSubobject<USphereComponent>(TEXT("OuterSphereComp"));
	InnerSphereComp = CreateDefaultSubobject<USphereComponent>(TEXT("InnerSphereComp"));

	RootComponent = MeshComp;
	MeshComp->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	InnerSphereComp->SetupAttachment(MeshComp);
	InnerSphereComp->SetSphereRadius(100);

	OuterSphereComp->SetupAttachment(MeshComp);
	OuterSphereComp->SetSphereRadius(3000);
	
	


	InnerSphereComp->OnComponentBeginOverlap.AddDynamic(this, &AFPSBlackHole::OnOverlapInnerSphere);
}

// Called when the game starts or when spawned
void AFPSBlackHole::BeginPlay()
{
	Super::BeginPlay();

}
void AFPSBlackHole::OnOverlapInnerSphere(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult &SweepResult)
{
	if (OtherActor)
	{
		OtherActor->Destroy();
	}
}
// Called every frame
void AFPSBlackHole::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	TArray<UPrimitiveComponent*> OverlappingComp;

	OuterSphereComp->GetOverlappingComponents(OverlappingComp);

	for (int i = 0; i < OverlappingComp.Num(); i++)
	{
		UPrimitiveComponent* OverlappedComp = OverlappingComp[i];
		if (OverlappedComp && OverlappedComp->IsSimulatingPhysics())
		{

			const float forceStrenght = -2000;
			const float sphereRadius = OuterSphereComp->GetScaledSphereRadius();

			OverlappedComp->AddRadialForce(this->GetActorLocation(), sphereRadius, forceStrenght, ERadialImpulseFalloff::RIF_Constant, true);
		}

	}
}

