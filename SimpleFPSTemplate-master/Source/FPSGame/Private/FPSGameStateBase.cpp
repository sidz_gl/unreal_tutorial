// Fill out your copyright notice in the Description page of Project Settings.

#include "FPSGameStateBase.h"
#include "FPSPlayerController.h"

void AFPSGameStateBase::MulticastOnCompletedMission_Implementation(APawn *InstigatorPawn, bool a_bMissionSucess)
{

	/*for (FConstPawnIterator it = GetWorld()->GetPawnIterator(); it; it++)
	{

		APawn *Pawn = it->Get();
		if (Pawn && Pawn->IsLocallyControlled())
		{
			Pawn->DisableInput(nullptr);
		}
	}*/
	for (FConstPlayerControllerIterator it = GetWorld()->GetPlayerControllerIterator(); it; it++)
	{
		AFPSPlayerController *PC = Cast<AFPSPlayerController>(it->Get());
		if (PC && PC->IsLocalController())
		{
			PC->CompletedMission(InstigatorPawn, a_bMissionSucess);

			APawn *Pawn = PC->GetPawn();
			if (Pawn)
			{
				Pawn->DisableInput(nullptr);
			}


		}
	}

}