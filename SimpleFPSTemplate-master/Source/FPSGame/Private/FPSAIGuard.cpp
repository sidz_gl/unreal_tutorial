// Fill out your copyright notice in the Description page of Project Settings.

#include "FPSAIGuard.h"
#include "Perception/PawnSensingComponent.h"
#include "DrawDebugHelpers.h"
#include "TimerManager.h"
#include "FPSGameMode.h"
#include "Kismet/GameplayStatics.h"

#include "AI/NavigationSystemBase.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"
#include "Net/UnrealNetwork.h"

// Sets default values
AFPSAIGuard::AFPSAIGuard()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	PawnSensingComp = CreateDefaultSubobject<UPawnSensingComponent>(TEXT("PawnSensingComp"));

	m_eGuardState = eAIState::IDLE;


}

void AFPSAIGuard::MoveToNextPatrolPoint()
{
	if (CurrentPatrolPoint == nullptr || CurrentPatrolPoint == SecondPatrolPoint)
	{
		CurrentPatrolPoint = FirstPatrolPoint;
	}
	else
	{
		CurrentPatrolPoint = SecondPatrolPoint;
	}
	//UNavigationSystem::SimpleMoveToActor(GetController(), CurrentPatrolPoint);
	UAIBlueprintHelperLibrary::SimpleMoveToActor(GetController(), CurrentPatrolPoint);


	UE_LOG(LogTemp, Log, TEXT("MoveToNextPatrolPoint::Moving to a PAtrol Point"));
}

void AFPSAIGuard::OnHearNoise(APawn* a_Pawn, const FVector& Location, float Volume)
{

	if (m_eGuardState == eAIState::ALERTED)
	{
		UE_LOG(LogTemp, Log, TEXT("OnHearNoise::Guard already Alerted!"));
		return;
	}
	UE_LOG(LogTemp, Log, TEXT("OnHearNoise heard by pawn!"));
	DrawDebugSphere(GetWorld(), Location, 32.0f, 12, FColor::Yellow, false, 10.0f);

	FVector direction = Location - GetActorLocation();
	direction.Normalize();
	FRotator newLookAt = FRotationMatrix::MakeFromX(direction).Rotator();
	newLookAt.Pitch = 0.0f;
	newLookAt.Roll = 0.0f;

	SetActorRotation(newLookAt);


	GetWorldTimerManager().ClearTimer(TimerHandleForIdle);
	GetWorldTimerManager().SetTimer(TimerHandleForIdle, this, &AFPSAIGuard::OnDetectTimeFinish, 3.0f);

	UpdateState(eAIState::SUSPICIOUS);
	bPatrol = false;
	GetController()->StopMovement();
}

void AFPSAIGuard::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AFPSAIGuard, m_eGuardState);
}
void AFPSAIGuard::OnRep_GuardState()
{
	OnGuardStateChange(m_eGuardState);

}
void AFPSAIGuard::UpdateState(eAIState a_eNewState)
{
	if (a_eNewState == m_eGuardState)
	{
		UE_LOG(LogTemp, Log, TEXT("Updating to Same State!"))
			return;
	}
	m_eGuardState = a_eNewState;
	OnRep_GuardState();

}


void AFPSAIGuard::OnAISeePawn(APawn* a_Pawn)
{
	if (m_eGuardState == eAIState::ALERTED)
	{
		UE_LOG(LogTemp, Log, TEXT("OnAISeePawn::Guard already Alerted!"));
		return;
	}

	UE_LOG(LogTemp, Log, TEXT("OnAISeePawn seen by pawn!"));
	if (a_Pawn == nullptr)
	{
		return;
	}
	DrawDebugSphere(GetWorld(), a_Pawn->GetActorLocation(), 32.0f, 12, FColor::Red, false, 10.0f);

	UE_LOG(LogTemp, Log, TEXT("OnAISeePawn:CAlling Game Mode Mission Complete"));
	AFPSGameMode* myGameMode = Cast<AFPSGameMode>(GetWorld()->GetAuthGameMode());
	if (myGameMode)
	{
		myGameMode->CompletedMission(a_Pawn, false);
	}
	UpdateState(eAIState::ALERTED);
	bPatrol = false;
	GetController()->StopMovement();

}

void AFPSAIGuard::OnDetectTimeFinish()
{
	if (m_eGuardState == eAIState::ALERTED)
	{
		UE_LOG(LogTemp, Log, TEXT("OnDetectTimeFinish::Guard already Alerted!"));
		return;
	}
	SetActorRotation(OrginalRotation);
	UpdateState(eAIState::IDLE);
	bPatrol = true;
	MoveToNextPatrolPoint();
}

// Called when the game starts or when spawned
void AFPSAIGuard::BeginPlay()
{
	Super::BeginPlay();
	PawnSensingComp->OnSeePawn.AddDynamic(this, &AFPSAIGuard::OnAISeePawn);
	PawnSensingComp->bEnableSensingUpdates = true;

	PawnSensingComp->OnHearNoise.AddDynamic(this, &AFPSAIGuard::OnHearNoise);
	OrginalRotation = GetActorRotation();

	if (bPatrol)
	{
		MoveToNextPatrolPoint();
	}

}

// Called every frame
void AFPSAIGuard::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (CurrentPatrolPoint)
	{
		//(GetWorld(), GetActorLocation(), 32.0f, 12, FColor::Yellow, false, 10.0f);
		//DrawDebugSphere(GetWorld(), CurrentPatrolPoint->GetActorLocation(), 32.0f, 12, FColor::Yellow, false, 10.0f);
	//	DrawDebugLine(GetWorld(), GetActorLocation(), CurrentPatrolPoint->GetActorLocation(), FColor::Black, 0.5f);
		FVector delta = GetActorLocation() - CurrentPatrolPoint->GetActorLocation();
		float distance = delta.Size();
		distance = FVector::Dist(GetActorLocation(), CurrentPatrolPoint->GetActorLocation());
		
		if (distance < 120)
		{
			MoveToNextPatrolPoint();
		//	UE_LOG(LogTemp, Log, TEXT("Close to next patrol point::Changing Patrol Point!!"));
		}
		else
		{
		//	UE_LOG(LogTemp, Log, TEXT("Distance to Patrol Point:%f"), distance);
		//	UE_LOG(LogTemp, Log, TEXT("Distance to Patrol Point result:%d"), (distance < 50));
		}
	}

}

// Called to bind functionality to input
void AFPSAIGuard::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

