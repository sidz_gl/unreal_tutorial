// Fill out your copyright notice in the Description page of Project Settings.

#include "FPSLaunchPad.h"
#include "Components/BoxComponent.h"
#include "FPSCharacter.h"
#include "Kismet/GameplayStatics.h"
#include "Components/MeshComponent.h"

// Sets default values
AFPSLaunchPad::AFPSLaunchPad()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	BoxComp = CreateDefaultSubobject<UBoxComponent>(TEXT("Box_Component"));
	BoxComp->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Overlap);
	BoxComp->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Overlap);
	BoxComp->OnComponentBeginOverlap.AddDynamic(this, &AFPSLaunchPad::OnHandleOverlap);

	MeshComp = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh_Component"));

	RootComponent = BoxComp;
	MeshComp->SetupAttachment(RootComponent);

	LaunchStrenght = 1500;
	LaunchPitchAngle = 35.0f;
}

void AFPSLaunchPad::OnHandleOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	FVector LaunchVelocity;
	FRotator LauchRotation = GetActorRotation();
	LauchRotation.Pitch += LaunchPitchAngle;

	LaunchVelocity = LauchRotation.Vector()*LaunchStrenght;

	UE_LOG(LogTemp, Log, TEXT("HandleOverlap:Overlapped with Extraction Zone!"));
	AFPSCharacter* myPawn = Cast<AFPSCharacter>(OtherActor);
	if (myPawn != nullptr)
	{
		UE_LOG(LogTemp, Log, TEXT("Launch Pad:HandleOverlap:Overlapped with Character!"));
		//Launch charater funtion call
		myPawn->LaunchCharacter(LaunchVelocity, true, true);
	}
	else
	{
		/*if (OtherCollidableActor == nullptr)
		{
			UE_LOG(LogTemp, Log, TEXT("Launch Pad:HandleOverlap:No other colliable actor found!"));
			return;
		}*/
		if (OtherComp && OtherComp->IsSimulatingPhysics())
		{
			OtherComp->AddImpulse(LaunchVelocity, NAME_None, true);
		}

		
	}
}

// Called when the game starts or when spawned
void AFPSLaunchPad::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFPSLaunchPad::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

