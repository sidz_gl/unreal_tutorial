// Fill out your copyright notice in the Description page of Project Settings.

#include "FPSExtractionZone.h"
#include "Components/BoxComponent.h"
#include "Components/DecalComponent.h"
#include  "FPSCharacter.h"
#include "FPSGameMode.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
AFPSExtractionZone::AFPSExtractionZone()
{

	BoxComp = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxComp"));


	BoxComp->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	BoxComp->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	BoxComp->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Overlap);

	BoxComp->OnComponentBeginOverlap.AddDynamic(this, &AFPSExtractionZone::HandleOverlap);
	BoxComp->SetHiddenInGame(false);
	BoxComp->SetBoxExtent(FVector(500));
	RootComponent = BoxComp;


	DecalComp = CreateDefaultSubobject<UDecalComponent>(TEXT("DecalComp"));
	DecalComp->SetupAttachment(RootComponent);
	DecalComp->DecalSize = FVector(500, 500, 500);

}

void AFPSExtractionZone::HandleOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent*  OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult &SweepResult)
{
	UE_LOG(LogTemp, Log, TEXT("HandleOverlap:Overlapped with Extraction Zone!"));

	AFPSCharacter* myPawn = Cast<AFPSCharacter>(OtherActor);

	if (myPawn == nullptr)
	{
		return;
	}
		//Disable input
		if (myPawn->IsCarryingObjective)
		{
			UE_LOG(LogTemp, Log, TEXT("AFPSExtractionZone:CAlling Game Mode Mission Complete"));
			AFPSGameMode* myGameMode = Cast<AFPSGameMode> (GetWorld()->GetAuthGameMode()) ;
			if (myGameMode)
			{
				myGameMode->CompletedMission(myPawn,true);
			}
			else
			{
				UE_LOG(LogTemp,Log,TEXT("AFPSExtractionZone:Game mode is NULL"));
			}
		}
		else
		{
			UE_LOG(LogTemp, Log, TEXT("AFPSExtractionZone:Get the Objective First!"));
			UGameplayStatics::PlaySound2D(this, OverlapMissSound);
		}
	

}



